# Makefile for vasm-legacy 2.7.50

NAME?=vasm-legacy
VERSION?=2.7.50
CWD?=$(PWD)
DESTDIR?=$(CWD)/vasm-legacy
DATAROOTDIR?=$(DESTDIR)/usr
DOCDIR?=$(DATAROOTDIR)/doc/$(NAME)-$(VERSION)
BINDIR?=$(DESTDIR)/bin
SBINDIR?=$(DESTDIR)/sbin
VVERNAME?=                              # make install fails if this is not set
TRUE=/bin/true
INSTALL=/usr/bin/install
BRAND=$(CWD)/brandit


all:
	$(TRUE)
	
install:
	$(INSTALL) -d $(BINDIR) $(DOCDIR) $(SBINDIR)
	$(INSTALL) -d $(DATAROOTDIR)/share/pixmaps $(DATAROOTDIR)/share/applications
	$(INSTALL) -m 755 -t $(BINDIR) bin/*
	$(INSTALL) -m 644 -t $(DOCDIR) doc/*
	$(INSTALL) -m 755 -t $(SBINDIR) sbin/*
	/bin/ln -s /etc/rc.d/service $(SBINDIR)/service
	$(INSTALL) -m 744 -t $(DATAROOTDIR)/share/pixmaps usr/share/pixmaps/*
	$(INSTALL) -m 744 -t $(DATAROOTDIR)/share/applications usr/share/applications/*
	cd $(DESTDIR)&& $(BRAND) $(VVERNAME)
	cd $(CWD)

configure:
	$(TRUE)

clean:
	$(TRUE)
