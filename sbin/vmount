#!/bin/sh
# @vasm : MOUNT
# @level: root
# @description: setup mount points (/etc/fstab)   
# 
# (c) Eko M. Budi, 2003
# (c) Vector Linux, 2003
#
# Released under GNU GPL

VDIR=$(dirname $0)

. $VDIR/vasm-functions

check_root

# Path to files
fstab=/etc/fstab

PARTITION="/dev/hda1"
MOUNT_POINT="/mnt/win"
OPTIONS="default"
DUMP_CHECK="1 2"


# $1=FSTYPE 
def_options()
{
   case $1 in
     vfat)
        echo "umask=0,quiet,shortname=mixed"
	;;
     reiserfs|ext2|ext3|jfx|xfs)
        echo "noatime"
	;;
     ntfs-3g)
	echo "defaults,allow_other,umask=0,users,nls=utf8,noexec"
	;;
	*)
        echo "defaults"
	;;
   esac
}

# $1=FSTYPE
def_order()
{
   case $1 in
     vfat)
        echo "0 0"
	;;
     reiserfs|ext2|ext3|jfx|xfs)
        echo "0 2"
	;;
     *)
        echo "0 0"
	;;
   esac
}

# $1=FSTYPE $2=DEVICE
def_mount()
{
   case $1 in
     vfat)
        echo "/mnt/win"
	;;
     *)
        echo "/mnt/${2##*/}"
	;;
   esac
}


build_addmenu()
{
    status=1
    while read LINE; do
	PART=`echo $LINE | cut -f1 -d ' '`
	if ! cat $fstab | grep -qe "^$PART "; then
	    SIZE=`echo $LINE | cut -f4 -d ' '`
	    TYPE=`echo $LINE | cut -f6- -d ' '`
	    echo "'$PART' '$TYPE  size=$SIZE MB' \\" >> $fmenu
	    status=0
	fi
    done
    return $status
}

menu_add() {
   TITLE="ADD PARTITION"
   TEXT="\nSelect the partition you want to mount on boot time:"
   DIMENSION="20 60 10"
   
   # missed raid device :(
   probepart 2>/dev/null | grep -e "^/dev" | grep -v " Ext'd" | \
     grep -v "raid autodetect" | sed 's/*/ /' > $freply
    
   echo '$DCMD --backtitle "$BACKTITLE" --title "$TITLE" \' > $fmenu
   echo '--menu "$TEXT" $DIMENSION \' >> $fmenu
    build_addmenu < $freply >> $fmenu
    if [ $? != 0 ]; then
	msgbox "No partition is available"
	return 0
    fi
    echo '2> $freply' >> $fmenu
    
    . $fmenu || return $?
    
    PARTITION=`cat $freply`
    
    #echo "partition=$PARTITION"
    umount /var/tmp/tmount &> /dev/null
    rm -rf /var/tmp/tmount &> /dev/null
    mkdir -p /var/tmp/tmount &> /dev/null
    if ! mount $PARTITION /var/tmp/tmount &>/dev/null; then
        yesnobox "Cannot mount $PARTITION, try to format it?" "ERROR" || return 255
	$vdid/vformat $PARTITION || return 255
	if ! mount $PARTITION /var/tmp/tmount &>/dev/null; then
	    msgbox "Still cannot mount $PARTITION. Aborting" "ERROR"
	    clean_exit 0
	fi
    fi
    LINE=`mount | grep -e "$PARTITION "`
    umount /var/tmp/tmount
    FSTYPE=`echo $LINE | cut -f5 -d ' '`
    if [ $FSTYPE = ntfs ];then
	    FSTYPE=ntfs-3g
    fi
    MOUNT_POINT=`def_mount $FSTYPE $PARTITION`
    return 0
}

menu_mount()
{
   TITLE="MOUNT POINT"
   TEXT="\nType the directory to mount $PARTITION"
   DIMENSION="9 60"

   $WCMD --backtitle "$BACKTITLE" --title "$TITLE" \
   --inputbox "$TEXT" $DIMENSION "$MOUNT_POINT" 2> $freply || return $?

   MOUNT_POINT=`cat $freply`
   
   if [ -z "$MOUNT_POINT" ]; then
      msgbox "Please type a proper mount point" "ERROR"
      return 255
   fi
   if [ "$MOUNT_POINT" = "/" ]; then
      msgbox "Sorry, cannot mount on root /" "ERROR"
      return 255
   fi
   if [ ! -d $MOUNT_POINT ]; then
      if ! mkdir -p $MOUNT_POINT &>/dev/null; then
        msgbox "Cannot create the directory" "ERROR"
	return 255
      fi
   fi
   if ! mount $PARTITION $MOUNT_POINT &>/dev/null; then
      if ! mkdir -p $MOUNT_POINT; then
        msgbox "Cannot mount $PARTITION to $MOUNT_POINT" "ERROR"
	return 255
      fi
   fi
   LINE=`mount | grep -e "$PARTITION "`
   umount $PARTITION
   FSTYPE=`echo $LINE | cut -f5 -d ' '`
    if [ $FSTYPE = ntfs ];then
	    FSTYPE=ntfs-3g
    fi
   OPTIONS=`def_options $FSTYPE`
}

menu_opt()
{
   TITLE="MOUNT OPTIONS"
   TEXT="\n
The $PARTITION uses $FSTYPE file system.\n
We will mount it with the following options.\n
You may change it if you want:"
   DIMENSION="12 60"

   $WCMD --backtitle "$BACKTITLE" --title "$TITLE" \
   --inputbox "$TEXT" $DIMENSION "$OPTIONS" 2> $freply || return $?
   
   OPTIONS=`cat $freply`
   
   ORDER=`def_order $FSTYPE`
}

menu_add_finish()
{
    infobox "Adding $PARTITION ..."
    echo "" >> $fstab
    echo "$PARTITION   $MOUNT_POINT  $FSTYPE  $OPTIONS $ORDER" >> $fstab
    sleep 3
}

########################################################################
build_delmenu()
{
    status="false"
    while read LINE; do
	PART=`echo $LINE | cut -f1 -d ' '`
	MOUNT_POINT=`echo $LINE | cut -f3 -d ' '`
	if [ "$MOUNT_POINT" != "/" ]; then
	    MOUNT=`echo $LINE | cut -f2 -d ' '`
	    TYPE=`echo $LINE | cut -f3 -d ' '`
	    echo "'$PART' '$MOUNT ($TYPE)' off \\" >> $fmenu
	    status="true"
	fi
    done
    $status
}

menu_del() {
   TITLE="REMOVE MOUNT PARTITION"
   TEXT="\nSelect the partition you do not want to mount anymore:"
   DIMENSION="20 60 10"
   
   cat $fstab | grep -e "^/dev/" | grep -ve ".* */ .*" | \
     grep -ve ".* *none .*" > $freply
   
   echo '$DCMD --backtitle "$BACKTITLE" --title "$TITLE" \' > $fmenu
   [ "$CMD" ] && echo '--separator " " \' >> $fmenu   
   echo '--checklist "$TEXT" $DIMENSION \' >> $fmenu
    build_delmenu < $freply >> $fmenu
    if [ $? != 0 ]; then
	msgbox "No partition is available for removing"
	return 255
    fi
    echo '2> $freply' >> $fmenu
    
    . $fmenu || return $?
    
    PARTITIONS=`cat $freply`
    echo "$PARTITIONS"
}

menu_del_finish()
{
    for PARTITION in $PARTITIONS; do
	infobox "Removing $PARTITION ..."
	if cat $fstab | grep -ve "^$PARTITION " > $fstab.new; then
	    mv -f $fstab.new $fstab
	fi
	sleep 3
    done
}

#########################################################################
menu_main()
{
while [ 1 ]; do
  DIMENSION="14 53 3"
  TITLE="MOUNT PARTITIONS"
  TEXT="\n
This menu allows you to register the partitions
that will be mounted automatically at boot time.\n\n
Please select what do you want to do:"

$DCMD --backtitle "$BACKTITLE" --title "$TITLE" --menu "$TEXT" $DIMENSION \
 "ADD"  "Add a new partition" \
 "DEL"  "Stop mounting a partition" \
 "MOUNT"  "mount all partitions in /etc/fstab" \
 "DONE" "enough with this menu" \
 2> $freply

   status=$?
   [ $status != 0 ] && return $status

    case `cat $freply` in
      ADD)
	(wizard menu_add menu_mount menu_opt menu_add_finish)
        ;;
      DEL)
	(wizard menu_del menu_del_finish)
	;;
      MOUNT)
	mount -a 
	;;
      *)
        clean_exit 0
	;;
    esac
done
}

####################################################################################
# MAIN PROGRAM

case $1 in
    --add)
	wizard menu_add menu_mount menu_opt menu_add_finish
        ;;
    --delete)
	wizard menu_del menu_del_finish
	;;    
    *)
	menu_main
	;;
esac
